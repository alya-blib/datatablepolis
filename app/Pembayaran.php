<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';
    protected $fillable = ['nomor_peserta', 'nomor_polis', 'tahun_polis', 'bulan_polis', 'nama_peserta', 'tanggal_lahir', 'tanggal_bayar'];
}
