<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['age', 'address', 'bio', 'user_id'];

    public  $timestamps = false;
}
