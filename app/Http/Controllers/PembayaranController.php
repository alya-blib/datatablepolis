<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Exports\PembayaranExport;
use Maatwebsite\Excel\Facades\Excel;
use\App\Pembayaran;
use DB;

class PembayaranController extends Controller
{
    public function create()
    {
        return view('pembayaran.create');
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $pembayaran = DB::table('pembayaran')
         ->orWhere('nama_peserta', 'LIKE', '%'.$search.'%')
         ->orWhere('nomor_peserta', 'LIKE', '%'.$search.'%')
         ->orWhere('nomor_polis', 'LIKE', '%'.$search.'%')
         ->orWhere('tanggal_bayar', 'LIKE', '%'.$search.'%')
         ->get();

        return view('pembayaran.index', compact('pembayaran'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nomor_peserta' => 'required',
            'nomor_polis' => 'required',
            'tahun_polis' => 'required',
            'bulan_polis' => 'required',
            'nama_peserta' => 'required',
            'tanggal_lahir' => 'required',
            'tanggal_bayar' => 'required',
        ]);

        DB::table('pembayaran')->insert(
            [
                'nomor_peserta' => $request['nomor_peserta'],
                'nomor_polis' => $request['nomor_polis'],
                'tahun_polis' => $request['tahun_polis'],
                'bulan_polis' => $request['bulan_polis'],
                'nama_peserta' => $request['nama_peserta'],
                'tanggal_lahir' => $request['tanggal_lahir'],
                'tanggal_bayar' => $request['tanggal_bayar']
            ]
         );
         Alert::alert('Berhasil', 'Berhasil Menambah Data Baru', 'Type');
         return redirect('/pembayaran');
    }
    public function index()
    {
        $pembayaran = DB::table('pembayaran')->get();
        return view('pembayaran.index', compact('pembayaran'));
    }

    public function edit($id)
    {
        $pembayaran = DB::table('pembayaran')->where('id', $id)->first();

        return view('pembayaran.edit', compact('pembayaran'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nomor_peserta' => 'required',
            'nomor_polis' => 'required',
            'tahun_polis' => 'required',
            'bulan_polis' => 'required',
            'nama_peserta' => 'required',
            'tanggal_lahir' => 'required',
            'tanggal_bayar' => 'required',
        ]);

        DB::table('pembayaran')
            ->where('id', $id)
            ->update(
                [
                    'nomor_peserta' => $request['nomor_peserta'],
                    'nomor_polis' => $request['nomor_polis'],
                    'tahun_polis' => $request['tahun_polis'],
                    'bulan_polis' => $request['bulan_polis'],
                    'nama_peserta' => $request['nama_peserta'],
                    'tanggal_lahir' => $request['tanggal_lahir'],
                    'tanggal_bayar' => $request['tanggal_bayar']
                ]
            );

        return redirect('/pembayaran');

    }

    public function destroy($id)
    {
        DB::table('pembayaran')->where('id', '=', $id)->delete();
        return redirect('/pembayaran');
    }

    public function export() 
    {
        return Excel::download(new PembayaranExport, 'pembayaran.xlsx');
    }

}
