<nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
          <li class="nav-item sidebar-user-actions">
              <div class="user-details">
                <div class="d-flex justify-content-between align-items-center">
                  <div>
                    <div class="d-flex align-items-center">
                    <div class="sidebar-profile-img">
                        <img src="{{('/assets/images/faces/face28.png')}}" alt="image">
                      </div>
                      <div class="sidebar-profile-text">
                      @auth
                      <a href="/profile" class="d-block">{{ Auth::user()->name }}</a>
                      @endauth

                    @guest
                      Belum Login
                    @endguest
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item nav-category">Main</li>
            <li class="nav-item">
              <a class="nav-link" href="/pembayaran">
                <span class="icon-bg"><i class="mdi mdi-table-large menu-icon"></i></span>
                <span class="menu-title">Table Data Polis</span>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="/data-table" aria-expanded="false" aria-controls="ui-basic">
                <span class="icon-bg"><i class="mdi mdi-crosshairs-gps menu-icon"></i></span>
                <span class="menu-title" >Table</span>
              </a>
            </li> -->
           
            <li class="nav-item sidebar-user-actions">
              <div class="sidebar-user-menu">
                <a href="/login" class="nav-link"><i class="mdi mdi-login menu-icon"></i>
                  <span class="menu-title">Log In</span></a>
              </div>
            </li>
            @auth
          <li class="nav-item bg-danger">     
            <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><i class="mdi mdi-logout menu-icon"></i>
             {{ __('Logout') }}
            </a>
  
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
            </form>
            </li>
  
            @endauth 
          </ul>
        </nav>