@extends('layout.master')

@section('judul')
    list Data Pembayaran <br>
    <a href="/pembayaran/create" class="btn btn-success mt-2">Tambah Data</a> <a href="/test-excel" class="btn btn-success mt-2">Download excel</a>
@endsection

@section('content')
<div class="container">
        <div class="col-md-4">
            <form action="/search" method="get">
                <div class="input-group">
                    <input type="search" name="search" class="form-control">
                    <span class="input-group-prepend">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </span>
                </div>
            </form>
        </div>
		<div class="card-box table-responsive">
            <table id="example1" class="table table-striped table-hover display">
                <thead>
                <tr>
                <th>#</th>
                <th>Nomor Peserta</th>
                <th>Nomor Polis</th>
                <th>Tahun Polis</th>
                <th>Bulan Polis</th>
                <th>Nama Peserta</th>
                <th>Tanggal Lahir</th>
                <th>Tanggal Bayar</th>
                <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($pembayaran as $key=>$value)
                <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->nomor_peserta}}</td>
                <td>{{$value->nomor_polis}}</td>
                <td>{{$value->tahun_polis}}</td>
                <td>{{$value->bulan_polis}}</td>
                <td>{{$value->nama_peserta}}</td>
                <td>{{$value->tanggal_lahir}}</td>
                <td>{{$value->tanggal_bayar}}</td>
                <td>
                                <a href="/pembayaran/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                                <form action="/pembayaran/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                </tr>
                @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                @endforelse    
                </tbody>
            </table>
</div>
@endsection