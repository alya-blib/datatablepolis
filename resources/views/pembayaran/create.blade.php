@extends('layout.master')

@section('judul')
    Tambah Data Pembayaran
@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Tambah Data</h4>
                    <form action="/pembayaran" method="POST">
                        @csrf
                      <div class="form-group">
                        <label>Nomor Peserta</label>
                        <input type="text" class="form-control" name="nomor_peserta" placeholder="Isi Nomor Peserta">
                        @error('nomor_peserta')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Nomor Polis</label>
                        <input type="text" class="form-control" name="nomor_polis" placeholder="Isi Nomor Polis">
                        @error('nomor_polis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Tahun Polis</label>
                        <input type="number" class="form-control" name="tahun_polis" placeholder="Isi Tahun Polis">
                        @error('tahun_polis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Bulan Polis</label>
                        <input type="text" class="form-control" name="bulan_polis" placeholder="Isi Bulan Polis">
                        @error('bulan_polis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Nama Peserta</label>
                        <input type="text" class="form-control" name="nama_peserta" placeholder="Isi Nama Peserta">
                        @error('nama_peserta')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tanggal_lahir" placeholder="Isi Tanggal Lahir">
                        @error('tanggal_lahir')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Tanggal Bayar</label>
                        <input type="date" class="form-control" name="tanggal_bayar" placeholder="Isi Tanggal Bayar">
                        @error('tanggal_bayar')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    </form>
                  </div>
                </div>
              </div>

@endsection

