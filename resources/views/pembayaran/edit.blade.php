@extends('layout.master')

@section('judul')
    Edit Data Pembayaran
@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Data</h4>
                    <form action="/pembayaran/{{$pembayaran->id}}" method="POST">
                        @csrf
                        @method('put')
                      <div class="form-group">
                        <label>Nomor Peserta</label>
                        <input type="text" class="form-control" value="{{$pembayaran->nomor_peserta}}" name="nomor_peserta" placeholder="Isi Nomor Peserta">
                        @error('nomor_peserta')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Nomor Polis</label>
                        <input type="text" class="form-control" value="{{$pembayaran->nomor_polis}}" name="nomor_polis" placeholder="Isi Nomor Polis">
                        @error('nomor_polis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Tahun Polis</label>
                        <input type="number" class="form-control" value="{{$pembayaran->tahun_polis}}" name="tahun_polis" placeholder="Isi Tahun Polis">
                        @error('tahun_polis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Bulan Polis</label>
                        <input type="text" class="form-control" value="{{$pembayaran->bulan_polis}}" name="bulan_polis" placeholder="Isi Bulan Polis">
                        @error('bulan_polis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Nama Peserta</label>
                        <input type="text" class="form-control" value="{{$pembayaran->nama_peserta}}" name="nama_peserta" placeholder="Isi Nama Peserta">
                        @error('nama_peserta')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" class="form-control" value="{{$pembayaran->tanggal_lahir}}" name="tanggal_lahir" placeholder="Isi Tanggal Lahir">
                        @error('tanggal_lahir')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label>Tanggal Bayar</label>
                        <input type="date" class="form-control" value="{{$pembayaran->tanggal_bayar}}" name="tanggal_bayar" placeholder="Isi Tanggal Bayar">
                        @error('tanggal_bayar')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                      </div>
                      <button type="submit" class="btn btn-primary mr-2">Update</button>
                    </form>
                  </div>
                </div>
              </div>

@endsection

