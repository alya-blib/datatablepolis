<?php
use App\Http\Controllers\PembayaranController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.home');
});


Route::get('/pembayaran/create', 'PembayaranController@create');
Route::post('/pembayaran', 'PembayaranController@store');
Route::get('/pembayaran', 'PembayaranController@index');
Route::get('/pembayaran/{pembayaran_id}/edit', 'PembayaranController@edit');
Route::put('/pembayaran/{pembayaran_id}', 'PembayaranController@update');
Route::delete('/pembayaran/{pembayaran_id}', 'PembayaranController@destroy');
Route::get('/search', 'PembayaranController@search');
Route::get('/test-excel', 'PembayaranController@export');
Auth::routes();


